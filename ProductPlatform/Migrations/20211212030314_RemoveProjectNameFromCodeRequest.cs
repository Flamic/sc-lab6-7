﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProductPlatform.Migrations
{
    public partial class RemoveProjectNameFromCodeRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProjectName",
                table: "CodeRequests");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ProjectName",
                table: "CodeRequests",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
