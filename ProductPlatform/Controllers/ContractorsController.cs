﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProductPlatform.Models;

namespace ProductPlatform.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContractorsController : ControllerBase
    {
        private readonly ProductPlatformContext _context;
        private readonly IMapper _mapper;

        public ContractorsController(ProductPlatformContext context)
        {
            _context = context;
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Contractor, ContractorViewModel>();
                cfg.CreateMap<EditContractorViewModel, Contractor>();
            });
            _mapper = new Mapper(config);
        }

        // GET: api/Contractors
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<Contractor>>> GetContractors()
        {
            return Ok(_mapper.Map<IEnumerable<Contractor>, List<ContractorViewModel>>(await _context.Contractors.ToListAsync()));
        }

        // GET: api/Contractors/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Contractor>> GetContractor([FromRoute] int id)
        {
            var contractor = await _context.Contractors.FindAsync(id);

            if (contractor == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<Contractor, ContractorViewModel>(contractor));
        }

        // PUT: api/Contractors/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PutContractor([FromRoute] int id, [FromBody] EditContractorViewModel contractor)
        {
            var model = _mapper.Map<EditContractorViewModel, Contractor>(contractor);
            model.ContractorID = id;
            _context.Entry(model).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContractorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return AcceptedAtAction("GetContractor", new { id = model.ContractorID }, _mapper.Map<Contractor, ContractorViewModel>(model));
        }

        // POST: api/Contractors
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Contractor>> PostContractor([FromBody] EditContractorViewModel contractor)
        {
            var model = _mapper.Map<EditContractorViewModel, Contractor>(contractor);
            _context.Contractors.Add(model);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetContractor", new { id = model.ContractorID }, _mapper.Map<Contractor, ContractorViewModel>(model));
        }

        // DELETE: api/Contractors/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Contractor>> DeleteContractor([FromRoute] int id)
        {
            var contractor = await _context.Contractors.FindAsync(id);
            if (contractor == null)
            {
                return NotFound();
            }

            _context.Contractors.Remove(contractor);
            await _context.SaveChangesAsync();

            return Ok(_mapper.Map<Contractor, ContractorViewModel>(contractor));
        }

        private bool ContractorExists([FromRoute] int id)
        {
            return _context.Contractors.Any(e => e.ContractorID == id);
        }
    }
}
