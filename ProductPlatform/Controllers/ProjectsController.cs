﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProductPlatform.Models;

namespace ProductPlatform.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly ProductPlatformContext _context;
        private readonly IMapper _mapper;

        public ProjectsController(ProductPlatformContext context)
        {
            _context = context;
            var config = new MapperConfiguration(cfg =>
            {
                var contractors = _context.Contractors.ToList();
                cfg.CreateMap<Project, ProjectViewModel>()
                    .ForMember("Contractor", prop => prop.MapFrom(project => contractors.Single(c => c.ContractorID == project.ContractorID).FullName));
                cfg.CreateMap<EditProjectViewModel, Project>();
            });
            _mapper = new Mapper(config);
        }

        // GET: api/Projects
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<Project>>> GetProjects()
        {
            return Ok(_mapper.Map<IEnumerable<Project>, List<ProjectViewModel>>(await _context.Projects.ToListAsync()));
        }

        // GET: api/Projects/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Project>> GetProject([FromRoute] int id)
        {
            var project = await _context.Projects.FindAsync(id);

            if (project == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<Project, ProjectViewModel>(project));
        }

        // PUT: api/Projects/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PutProject([FromRoute] int id, [FromBody] EditProjectViewModel project)
        {
            var model = _mapper.Map<EditProjectViewModel, Project>(project);
            model.ProjectID = id;
            _context.Entry(model).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProjectExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return AcceptedAtAction("GetProject", new { id = model.ProjectID }, _mapper.Map<Project, ProjectViewModel>(model));
        }

        // POST: api/Projects
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Project>> PostProject([FromBody] EditProjectViewModel project)
        {
            var model = _mapper.Map<EditProjectViewModel, Project>(project);
            _context.Projects.Add(model);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProject", new { id = model.ProjectID }, _mapper.Map<Project, ProjectViewModel>(model));
        }

        // DELETE: api/Projects/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Project>> DeleteProject([FromRoute] int id)
        {
            var project = await _context.Projects.FindAsync(id);
            if (project == null)
            {
                return NotFound();
            }

            _context.Projects.Remove(project);
            await _context.SaveChangesAsync();

            return Ok(_mapper.Map<Project, ProjectViewModel>(project));
        }

        private bool ProjectExists([FromRoute] int id)
        {
            return _context.Projects.Any(e => e.ProjectID == id);
        }
    }
}
