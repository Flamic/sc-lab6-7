﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ProductPlatform.Models;

namespace ProductPlatform.Controllers
{
    public class TablesController : Controller
    {
        private readonly ProductPlatformContext _context;
        private readonly IMapper _mapper;

        public TablesController(ProductPlatformContext context)
        {
            _context = context;
            var config = new MapperConfiguration(cfg =>
            {
                var contractors = _context.Contractors.ToList();
                var customers = _context.Customers.ToList();
                var projects = _context.Projects.ToList();
                cfg.CreateMap<Customer, CustomerViewModel>();
                cfg.CreateMap<Contractor, ContractorViewModel>();
                cfg.CreateMap<Project, ProjectViewModel>()
                    .ForMember("Contractor", prop => prop.MapFrom(project => contractors.Single(c => c.ContractorID == project.ContractorID).FullName));
                cfg.CreateMap<CodeRequest, CodeRequestViewModel>()
                    .ForMember("Customer", prop => prop.MapFrom(cr => customers.Single(c => c.CustomerID == cr.CustomerID).FullName))
                    .ForMember("Project", prop => prop.MapFrom(cr => projects.Single(p => p.ProjectID == cr.ProjectID).Name));
            });
            _mapper = new Mapper(config);
        }

        [HttpGet]
        [Route("tables")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route("tables/customers")]
        public ActionResult Customers()
        {
            var data = _mapper.Map<IEnumerable<Customer>, List<CustomerViewModel>>(_context.Customers);
            return View(data);
        }

        [HttpGet]
        [Route("tables/contractors")]
        public ActionResult Contractors()
        {
            var data = _mapper.Map<IEnumerable<Contractor>, List<ContractorViewModel>>(_context.Contractors);
            return View(data);
        }

        [HttpGet]
        [Route("tables/projects")]
        public ActionResult Projects()
        {
            var data = _mapper.Map<IEnumerable<Project>, List<ProjectViewModel>>(_context.Projects);
            return View(data);
        }

        [HttpGet]
        [Route("tables/coderequests")]
        public ActionResult CodeRequests()
        {
            var data = _mapper.Map<IEnumerable<CodeRequest>, List<CodeRequestViewModel>>(_context.CodeRequests);
            return View(data);
        }
    }
}
