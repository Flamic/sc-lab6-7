﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProductPlatform.Models;

namespace ProductPlatform.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CodeRequestsController : ControllerBase
    {
        private readonly ProductPlatformContext _context;
        private readonly IMapper _mapper;

        public CodeRequestsController(ProductPlatformContext context)
        {
            _context = context;
            var config = new MapperConfiguration(cfg =>
            {
                var customers = _context.Customers.ToList();
                var projects = _context.Projects.ToList();
                cfg.CreateMap<CodeRequest, CodeRequestViewModel>()
                    .ForMember("Customer", prop => prop.MapFrom(cr => customers.Single(c => c.CustomerID == cr.CustomerID).FullName))
                    .ForMember("Project", prop => prop.MapFrom(cr => projects.Single(p => p.ProjectID == cr.ProjectID).Name));
                cfg.CreateMap<EditCodeRequestViewModel, CodeRequest>();
            });
            _mapper = new Mapper(config);
        }

        // GET: api/CodeRequests
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<CodeRequest>>> GetCodeRequests()
        {
            return Ok(_mapper.Map<IEnumerable<CodeRequest>, List<CodeRequestViewModel>>(await _context.CodeRequests.ToListAsync()));
        }

        // GET: api/CodeRequests/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CodeRequest>> GetCodeRequest([FromRoute] int id)
        {
            var codeRequest = await _context.CodeRequests.FindAsync(id);

            if (codeRequest == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<CodeRequest, CodeRequestViewModel>(codeRequest));
        }

        // PUT: api/CodeRequests/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PutCodeRequest([FromRoute] int id, [FromBody] EditCodeRequestViewModel codeRequest)
        {
            var model = _mapper.Map<EditCodeRequestViewModel, CodeRequest>(codeRequest);
            model.CodeRequestID = id;
            _context.Entry(model).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CodeRequestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return AcceptedAtAction("GetCodeRequest", new { id = model.CodeRequestID }, _mapper.Map<CodeRequest, CodeRequestViewModel>(model));
        }

        // PATCH: api/CodeRequests/5
        [HttpPatch("{id}")]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PatchCodeRequest([FromRoute] int id, [FromBody] EditCodeRequestViewModel codeRequest)
        {
            var request = await _context.CodeRequests.FindAsync(id);
            if (request == null) return NotFound();
            if (codeRequest.CustomerID != 0) request.CustomerID = codeRequest.CustomerID;
            if (codeRequest.ProjectID!= 0) request.ProjectID = codeRequest.ProjectID;
            if (codeRequest.Description != null) request.Description = codeRequest.Description;
            request.Accepted = codeRequest.Accepted;
            
            _context.Entry(request).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CodeRequestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return AcceptedAtAction("GetCodeRequest", new { id = request.CodeRequestID }, _mapper.Map<CodeRequest, CodeRequestViewModel>(request));
        }

        // POST: api/CodeRequests
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<CodeRequest>> PostCodeRequest([FromBody] EditCodeRequestViewModel codeRequest)
        {
            var model = _mapper.Map<EditCodeRequestViewModel, CodeRequest>(codeRequest);
            _context.CodeRequests.Add(model);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCodeRequest", new { id = model.CodeRequestID }, _mapper.Map<CodeRequest, CodeRequestViewModel>(model));
        }

        // DELETE: api/CodeRequests/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CodeRequest>> DeleteCodeRequest([FromRoute] int id)
        {
            var codeRequest = await _context.CodeRequests.FindAsync(id);
            if (codeRequest == null)
            {
                return NotFound();
            }

            _context.CodeRequests.Remove(codeRequest);
            await _context.SaveChangesAsync();

            return Ok(_mapper.Map<CodeRequest, CodeRequestViewModel>(codeRequest));
        }

        private bool CodeRequestExists([FromRoute] int id)
        {
            return _context.CodeRequests.Any(e => e.CodeRequestID == id);
        }
    }
}
