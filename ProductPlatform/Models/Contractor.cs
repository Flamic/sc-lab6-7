﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProductPlatform.Models
{
    public class Contractor : User
    {
        public int ContractorID { get; set; }

        [MaxLength(50)]
        public string Company { get; set; }

        [MaxLength(1000)]
        public string Portfolio { get; set; }

        public ICollection<Project> Projects { get; set; }
    }
}