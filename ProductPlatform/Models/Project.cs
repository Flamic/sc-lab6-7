﻿using System.Collections.Generic;

namespace ProductPlatform.Models
{
    public class Project
    {
        public int ProjectID { get; set; }
        public int ContractorID { get; set; }
        public Contractor Contractor { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }

        public ICollection<CodeRequest> CodeRequests { get; set; }
    }
}