﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProductPlatform.Models
{
    public class Customer : User
    {
        public int CustomerID { get; set; }

        [MaxLength(1000)]
        public string About { get; set; }

        public ICollection<CodeRequest> CodeRequests { get; set; }
    }
}