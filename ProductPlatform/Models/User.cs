﻿using System.ComponentModel.DataAnnotations;

namespace ProductPlatform.Models
{
    public class User
    {
        [Required]
        [StringLength(50, MinimumLength = 1)]
        [RegularExpression(@"^[A-z-]+$", ErrorMessage = "Name must contain only latin letters and dashes")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 1)]
        [RegularExpression(@"^[A-z-]+$", ErrorMessage = "Name must contain only latin letters and dashes")]
        public string SecondName { get; set; }

        public string FullName => $"{FirstName} {SecondName}";

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 8)]
        public string Password { get; set; }
    }
}