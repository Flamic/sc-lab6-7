﻿namespace ProductPlatform.Models
{
    public class CodeRequest
    {
        public int CodeRequestID { get; set; }
        public int ProjectID { get; set; }
        public Project Project { get; set; }
        public int CustomerID { get; set; }
        public Customer Customer { get; set; }
        public string Description { get; set; }
        public bool Accepted { get; set; }
    }
}