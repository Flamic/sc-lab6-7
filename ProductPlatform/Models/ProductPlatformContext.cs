﻿using Microsoft.EntityFrameworkCore;

namespace ProductPlatform.Models
{
    public class ProductPlatformContext : DbContext
    {
        public ProductPlatformContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Contractor> Contractors { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CodeRequest> CodeRequests { get; set; }
        public DbSet<Project> Projects { get; set; }
    }
}