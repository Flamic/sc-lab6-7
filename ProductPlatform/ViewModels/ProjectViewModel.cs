﻿using System.Collections.Generic;

namespace ProductPlatform.Models
{
    public class ProjectViewModel
    {
        public int ProjectID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Contractor { get; set; }
    }
}