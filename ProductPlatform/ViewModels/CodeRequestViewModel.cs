﻿namespace ProductPlatform.Models
{
    public class CodeRequestViewModel
    {
        public int CodeRequestID { get; set; }
        public string Project { get; set; }
        public string Customer { get; set; }
        public string Description { get; set; }
        public bool Accepted { get; set; }
    }
}