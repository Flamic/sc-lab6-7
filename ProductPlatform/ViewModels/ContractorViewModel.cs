﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProductPlatform.Models
{
    public class ContractorViewModel : UserViewModel
    {
        public int ContractorID { get; set; }
        public string Company { get; set; }
        public string Portfolio { get; set; }
    }
}