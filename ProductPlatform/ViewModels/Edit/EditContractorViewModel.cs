﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProductPlatform.Models
{
    public class EditContractorViewModel : EditUserViewModel
    {
        public string Company { get; set; }
        public string Portfolio { get; set; }
    }
}