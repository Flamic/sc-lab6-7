﻿using System.Collections.Generic;

namespace ProductPlatform.Models
{
    public class EditProjectViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public int ContractorID { get; set; }
    }
}