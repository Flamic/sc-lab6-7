﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProductPlatform.Models
{
    public class EditCustomerViewModel : EditUserViewModel
    {
        public string About { get; set; }
    }
}