﻿namespace ProductPlatform.Models
{
    public class EditCodeRequestViewModel
    {
        public int ProjectID { get; set; }
        public int CustomerID { get; set; }
        public string Description { get; set; }
        public bool Accepted { get; set; }
    }
}