﻿using System.ComponentModel.DataAnnotations;

namespace ProductPlatform.Models
{
    public class UserViewModel
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}