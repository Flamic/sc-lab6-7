﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProductPlatform.Models
{
    public class CustomerViewModel : UserViewModel
    {
        public int CustomerID { get; set; }
        public string About { get; set; }
    }
}